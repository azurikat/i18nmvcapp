﻿using I18nWebApp.Classes.I18N;
using System.Web.Mvc;

namespace I18nWebApp.Controllers
{
	public class HomeController : I18NController
	{
		public ActionResult Index()
		{
			ViewBag.Message = T("Modify this template to jump-start your ASP.NET MVC application.");

			return View();
		}

		public ActionResult About()
		{
			ViewBag.Message = T("Your app description page.");

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = T("Your contact page.");

			return View();
		}
	}
}
