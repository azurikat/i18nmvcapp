﻿namespace I18nWebApp.Classes.ViewEngine
{
	using I18nWebApp.Classes.I18N;
	using I18NProvider = I18nWebApp.Classes.I18N.I18N;

	public abstract class I18nViewPage<TEntity> : System.Web.Mvc.WebViewPage<TEntity>
	{
		private static readonly II18N localeProvider = new I18NProvider();
		private TDelegate defaultLoc = localeProvider.Localizer;

		public TDelegate T
		{
			get { return defaultLoc; }
			set { defaultLoc = value; }
		}
	}

	/// <summary>
	/// Abstract dinamic page
	/// </summary>
	public abstract class I18nViewPage : I18nViewPage<dynamic>
	{

	}
}
