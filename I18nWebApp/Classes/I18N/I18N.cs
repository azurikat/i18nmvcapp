﻿namespace I18nWebApp.Classes.I18N
{
	using I18nWebApp.Classes;
	using System.Collections.Generic;
	using System.Globalization;
	using System.IO;
	using System.Web;
	using System.Xml.XPath;

	/// <summary>
	/// Делегат перевода
	/// </summary>
	/// <param name="key"></param>
	/// <param name="parameters"></param>
	/// <returns></returns>
	public delegate string TDelegate(string key, params object[] parameters);

	/// <summary>
	/// Класс локализации
	/// </summary>
	public sealed class I18N : II18N
	{

		public static TDelegate NullLocalizer = (s, o) => { return string.Format(s, o); };

		#region Fields

		public const string DEFAULTCULTURENAME = "ru-ru";
		private IList<LocaleItem> i18nDescription = new List<LocaleItem>();
		private CultureInfo currentCulture;
		/// <summary>
		/// Строки переводов
		/// </summary>
		private IXPathNavigable translationStrings;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Сервис локализаций
		/// </summary>
		public I18N()
		{
			XmlInfoPath = HttpContext.Current.Server.MapPath("~/App_Data\\I18N\\I18N.xml");
			XPathDocument i18nDescr = new XPathDocument(File.OpenRead(XmlInfoPath));
			XPathNodeIterator nodes = i18nDescr.CreateNavigator().Select("/I18N/locale");
			i18nDescription = new List<LocaleItem>();

			while (nodes.MoveNext())
			{
				i18nDescription.Add(new LocaleItem(nodes.Current));
			}

			LoadTranslation();

			Localizer = new TDelegate(T);
		}

		/// <summary>
		/// Загружаем список переводов
		/// </summary>
		public void LoadTranslation()
		{
			if (currentCulture != CultureInfo.CurrentCulture)
			{
				currentCulture = CultureInfo.CurrentCulture;
				CultureName = currentCulture.ToString().ToLower();
				XmlPath = HttpContext.Current.Server.MapPath("~/App_Data\\I18N\\{0}.xml");

				if (!File.Exists(string.Format(XmlPath, CultureName))) { CultureName = DEFAULTCULTURENAME; }

				if (File.Exists(string.Format(XmlPath, CultureName)))
				{
					XmlPath = string.Format(XmlPath, CultureName);
					//Загружаем локализацию
					translationStrings = new XPathDocument(File.OpenRead(XmlPath));
					//Загружаем описание
				}
				else
				{
					translationStrings = new XPathDocument(new StringReader("<root><info></info><string><key name=\"\"></key></string></root>"));
				}
			}
		}

		#endregion Constructors

		#region Properties
		
		/// <summary>
		/// Текущая культура
		/// </summary>
		public CultureInfo CultureInfo { get; set; }
		
		/// <summary>
		/// Имя текущей локализация
		/// </summary>
		public string CultureName { get; private set; }

		/// <summary>
		/// Путь к описанию
		/// </summary>
		public string XmlInfoPath { get; set; }

		/// <summary>
		/// Путь текущей локализации
		/// </summary>
		public string XmlPath { get; private set; }

		#endregion Properties

		#region Methods

		/// <summary>
		/// Возвращает список локализаций
		/// </summary>
		/// <returns></returns>
		public IList<LocaleItem> I18NList()
		{
			return i18nDescription;
		}

		/// <summary>
		/// Translator
		/// </summary>
		public TDelegate Localizer { get; set; }

		/// <summary>
		/// Выводим строку по ее ключу (форматированный вывод)
		/// </summary>
		/// <param name="key">ключ</param>
		/// <param name="args">параметры подстановки</param>
		/// <returns>сформированная строка</returns>
		public string T(string key, params object[] args)
		{
			return string.Format(currentCulture, getString(key), args);
		}

		private string getString(string key)
		{
			var result = translationStrings
				.CreateNavigator()
				.SelectSingleNode(string.Format(currentCulture, "/root/string/key[@name='{0}']", key));

			if (result != null)
			{
				return result.Value;
			}

			return key;
		}

		#endregion Methods
	}
}